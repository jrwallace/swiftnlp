//
//  Boruvka.swift
//  SwiftNLP
//
//  Created by Jim Wallace on 2024-10-24.
//

import Foundation
import Synchronization

class SynchronizedEdgeList: @unchecked Sendable {
    private var value: [Edge?]
    private let write: Mutex<Bool> = Mutex(false)
    
    init(count: Int) {
        self.value = Array(repeating: nil, count: count)
    }
    
    func getValue() -> [Edge?] {
        return value
    }
    
    func setValue(newValue: [Edge?]) {
        write.withLock { _ in
            self.value = newValue
        }
    }
    
    func updateCheapest(vertex: Int, edge: Edge) {
        write.withLock { _ in
            if value[vertex] == nil || edge.weight < value[vertex]!.weight {
                value[vertex] = edge
            }
        }
    }
    
    func getCheapest(vertex: Int) -> Edge? {
        return value[vertex]
    }
}

extension HDBScan {
    
    func boruvkasMST<C: SNLPIndexedCorpus & Sendable>(_ corpus: C, maxConcurrentTasks: Int = max(1, ProcessInfo.processInfo.activeProcessorCount - 2)) async -> [Edge] where C.Item: Sendable {
        var mst: [Edge] = []
        let uf = UnionFind(size: Int(corpus.count))
        
        let numVertices = Int(corpus.count)
        var numComponents = Int(corpus.count)
        
        let cheapest = SynchronizedEdgeList(count: numVertices)
        
        
        while numComponents > 1 {
            cheapest.setValue(newValue: Array(repeating: nil, count: numVertices)) // Reset cheapest edges
            
            await withTaskGroup(of: Void.self) { group in
                let chunkSize = (numVertices + maxConcurrentTasks - 1) / maxConcurrentTasks
                
                for chunkStart in stride(from: 0, to: numVertices, by: chunkSize) {
                    let chunkEnd = min(chunkStart + chunkSize, numVertices)
                    group.addTask {
                        for vertex in chunkStart ..< chunkEnd {
                            // Query the ANN index for neighbors
                            let neighbors = await corpus.index.find(near: corpus.encodedDocuments[vertex], limit: minimumNeighbours).filter { $0 != -1 }
                            
                            for neighbor in neighbors {
                                let rootU = uf.find(vertex)
                                let rootV = uf.find(neighbor)
                                let weight = await mutualReachabilityDistance(neighbor, vertex, corpus: corpus)
                                
                                // Only consider edges between different components
                                if rootU != rootV {
                                    let edge = Edge(a: vertex, b: neighbor, weight: weight)
                                    cheapest.updateCheapest(vertex: rootU, edge: edge)
                                }
                            }
                        }
                    }
                }
            }
            
            // Add the cheapest edges to the MST and union the components
            let currentCheapest = cheapest.getValue()
            var unionsMade = false
            for i in 0..<numVertices {
                if let edge = currentCheapest[i] {
                    let rootU = uf.find(edge.a)
                    let rootV = uf.find(edge.b)
                    
                    // Only add the edge if it connects different components
                    if rootU != rootV {
                        mst.append(edge)
                        uf.union(rootU, rootV)
                        numComponents -= 1
                        unionsMade = true
                    }
                }
            }

            // Global fallback: search for the best edge across all components
            if !unionsMade {
                print("Global Fallback!")
                var bestEdge: Edge? = nil
                for vertex in 0..<numVertices {
                    for other in 0..<numVertices where uf.find(vertex) != uf.find(other) {
                        let distance = await mutualReachabilityDistance(vertex, other, corpus: corpus)
                        if bestEdge == nil || distance < bestEdge!.weight {
                            bestEdge = Edge(a: vertex, b: other, weight: distance)
                        }
                    }
                }
                
                if let bestEdge = bestEdge {
                    mst.append(bestEdge)
                    uf.union(uf.find(bestEdge.a), uf.find(bestEdge.b))
                    numComponents -= 1
                }
            }

        }
        
        return mst
    }
}
