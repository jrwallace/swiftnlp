// Copyright (c) 2024 Jim Wallace
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

extension Dendrogram {
    
    func condense(minClusterSize: Int) {
        guard let root = root else { return }
        
        var stack: [DendroNode] = [root]
        var outputStack: [DendroNode] = []
        
        // First traversal: Build post-order stack
        while !stack.isEmpty {
            let currentNode = stack.removeLast()
            outputStack.append(currentNode)
            
            // Push children onto the stack to be visited before the current node
            if let children = currentNode.children {
                for child in children {
                    stack.append(child)
                }
            }
        }
        
        // Second phase: Process nodes in post-order for condensation
        while !outputStack.isEmpty {
            let currentNode = outputStack.removeLast()
            
            if let children = currentNode.children {
                for child in children {
                    // If the child is too small, merge it and update stability
                    if child.points.count < minClusterSize {
                        currentNode.merge(with: child)
                    }
                }
                
                // If only one child remains, merge it and update stability
                if currentNode.children!.count == 1 {
                    let child = currentNode.children!.first!
                    currentNode.merge(with: child)
                }
            }
        }
    }


    func extractClusters() -> Set<DendroNode> {
        guard let root = root else { return [] }
        
        var stack: [DendroNode] = [root]
        var outputStack: [DendroNode] = []
        var selectedClusters: Set<DendroNode> = []
        
        // First traversal: Build post-order stack
        while !stack.isEmpty {
            let currentNode = stack.removeLast()
            outputStack.append(currentNode)
            
            // Push children onto the stack to be visited before the current node
            if let children = currentNode.children {
                for child in children {
                    stack.append(child)
                }
            } else {
                // Leaf node is a selected cluster initially
                selectedClusters.insert(currentNode)
            }
        }
        
        // Second phase: Process nodes in post-order for cluster extraction
        while !outputStack.isEmpty {
            let currentNode = outputStack.removeLast()
            
            if let children = currentNode.children {
                // Calculate the sum of child stabilities
                let sumOfChildStabilities = children.reduce(0) { sum, child in sum + child.stability }
                
                if sumOfChildStabilities > currentNode.stability {
                    // Propagate child stabilities to the current node
                    currentNode.stability = sumOfChildStabilities
                } else {
                    // Current node's stability is greater, so select this node and unselect descendants
                    selectedClusters.insert(currentNode)
                    for child in children {
                        selectedClusters.remove(child)
                    }
                }
            }
        }
        
        return selectedClusters
    }
    
}
