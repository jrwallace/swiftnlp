import MLX
import MLXOptimizers
import MLXRandom
import MLXNN
import MLXLinalg

/// UMAP Module that learns a low-dimensional embedding of data.
class UMAPModule: Module {
    
    // Trainable embedding matrix (shape: [n_samples, n_components])
    var embedding: MLXArray
    let distanceMetric: DistanceMetric

    
    let epsilon: Float = 1e-7  // Small constant for numerical stability.
    
    // Fuzzy neighbor graph (high-dimensional relationships)
    let fuzzyComplex: FuzzySimplicialComplex
    let numNeighbours: Int
    let negativeSampleRate: Int
    let corpus: any SNLPIndexedCorpus
    
    // UMAP hyperparameters
    let a: MLXArray
    let b: MLXArray

    // Internal variables for computations
    let attract_i: MLXArray
    let attract_j: MLXArray
    let attract_w: MLXArray
    
    init<C: SNLPIndexedCorpus>(
        _ corpus: inout C,
        targetDimensions: Int,
        numNeighbours: Int,
        negativeSampleRate: Int = 5,
        initialValues: InitialValues = .random,
        distanceMetric: DistanceMetric = CosineSimilarity(),
        a: Float = 1.577,
        b: Float = 0.895
    ) async {
                
        // 0. Generate an FSC for this corpus in high-dimensional space
        fuzzyComplex = await FuzzySimplicialComplex(corpus, k: numNeighbours)
        self.numNeighbours = numNeighbours
        self.negativeSampleRate = negativeSampleRate
        self.corpus = corpus
        
        // 1. Initialize embedding with small random values.
        switch initialValues {
        case .laplacian:
            let laplacian = LaplacianReducer(targetDimensions: targetDimensions, fsc: fuzzyComplex)
            laplacian.reduce(&corpus.encodedDocumentsAsMLXArray)
            embedding = corpus.encodedDocumentsAsMLXArray
            
        case .pca:
            let pca = PCA(targetDimensions: targetDimensions)
            pca.reduce(&corpus.encodedDocumentsAsMLXArray)
            embedding = corpus.encodedDocumentsAsMLXArray
            
        case .random:
            embedding = MLXRandom.normal([corpus.count, targetDimensions], dtype: .bfloat16)
        }
        
        
        // 2. Convert neighbor pairs to MLXArrays.
        var neighborPairs: [(Int, Int, Float)] = []
        for edges in fuzzyComplex.neighbours {
            for edge in edges {
                neighborPairs.append((edge.a, edge.b, edge.weight))
            }
        }
        attract_i = neighborPairs.map { Int32($0.0) }.asMLXArray(dtype: .int32)
        attract_j = neighborPairs.map { Int32($0.1) }.asMLXArray(dtype: .int32)
        attract_w = neighborPairs.map { $0.2 }.asMLXArray(dtype: .float32)
        
        // 3. Convert hyperparameters to scalar MLXArrays.
        self.a = MLXArray(a)
        self.b = MLXArray(b)
        
        self.distanceMetric = distanceMetric
        
        // Initialize base Module (registers parameters)
        super.init()
    }
}

extension UMAPModule {

    /// Compute the UMAP cross-entropy loss for the current embedding.
    /// - Returns: A scalar MLXArray representing the loss value.
    func attractiveLoss(_ input: [MLXArray]) -> [MLXArray] {
        
        assert( input.count == 1 )
        
        let input = input[0]
        let pos_i = input[attract_i]  // shape: [numAttract, nDims]
        let pos_j = input[attract_j]  // shape: [numAttract, nDims]
        
        // Compute squared distances using the generic batchDistance function.
        var distSq = distanceMetric.batchDistance(between: pos_i, pos_j)
        if distanceMetric.faissMetric == .innerProduct {
            distSq = distSq / Float.pi
        }
        
        // Attractive Forces: q_ij = 1 / (1 + a * (distance^(2))^b)
        let q_ij = 1.0 / (1.0 + a * distSq.pow(b))
        let safe_q_ij = clip(q_ij, min: epsilon, max: 1.0 - epsilon)
        let attractLoss = -(safe_q_ij.log() * attract_w).sum()
     
        return [attractLoss]
    }
        
        
        /// Compute the UMAP cross-entropy loss for the current embedding.
        /// - Returns: A scalar MLXArray representing the loss value.
    func repulsiveLoss(_ input: [MLXArray]) -> [MLXArray] {
        
        assert( input.count == 1 )
        
        // Generate negative samples
        var negativeSamples: [(Int, Int)] = []
        
        for idx in 0 ..< numNeighbours {
            var sampledPoints = Set<Int>()
            while sampledPoints.count < negativeSampleRate {
                let j = Int.random(in: 0 ..< corpus.count)
                if j != idx && !fuzzyComplex.isNeighbor(idx, j) && !fuzzyComplex.isNeighbor(j, idx) {
                    sampledPoints.insert(j)
                }
            }
            negativeSamples.append(contentsOf: sampledPoints.map{ (idx, $0) })
        }
        let repel_i = negativeSamples.map { Int32($0.0) }.asMLXArray(dtype: .int32)
        let repel_j = negativeSamples.map { Int32($0.1) }.asMLXArray(dtype: .int32)

        
        let input = input[0]
        let pos_iNeg = input[repel_i] // [numNeg, nDims]
        let pos_kNeg = input[repel_j] // [numNeg, nDims]
        
        // Compute squared distances for repulsion.
        let distSqNeg = distanceMetric.batchDistance(between: pos_iNeg, pos_kNeg)
        
        // UMAP formulation for repulsion: q_ik = 1 / (1 + a * (distSqNeg)^b)
        let q_ik = 1.0 / (1.0 + a * distSqNeg.pow(b))
        let safeOneMinusQik = clip( (1.0 - q_ik), min: epsilon, max: 1.0 - epsilon)
        let repelLoss = -safeOneMinusQik.log().sum()
        
        return [repelLoss]
    }


    
    
    /// Optimize the embedding using a given MLX optimizer for a number of epochs.
    func optimizeEmbedding<Opt: OptimizerBase<MLXArray>>(optimizer: Opt, epochs: Int, tolerance: Float = 1e-4) {
        var state = optimizer.newState(parameter: embedding)
        let attractiveLG = valueAndGrad(attractiveLoss)
        let repusliveLG = valueAndGrad(repulsiveLoss)
        var previousLoss: Float = 0.0

        var attractAlpha = MLXArray(1.0)
        var repulsiveAlpha = MLXArray(1.0)
        
        
        //makeScatterPlot(embedding, dataSetName: "GPUMAP_INIT")
        
        for epoch in 1 ..< epochs {
            
            let (av, ag) = attractiveLG([self.embedding])
            let (rv, rg) = repusliveLG([self.embedding])
            
            let currentLoss = av[0].item(Float.self) + rv[0].item(Float.self)
            let deltaLoss = abs(currentLoss - previousLoss)
            if deltaLoss < tolerance {
                break
            }
            previousLoss = currentLoss
            
            // Original UMAP implementation
            // Apply decay factor to both attractive and repulsive gradients equally
            // https://arxiv.org/pdf/1802.03426
            attractAlpha = MLXArray(1.0)
            repulsiveAlpha = MLXArray(1.0 - ( Float(epoch) / Float(epochs) ))
            let gradient = attractAlpha * ag.first! + repulsiveAlpha * rg.first!
            
            (self.embedding, state) = optimizer.applySingle(gradient: gradient, parameter: self.embedding, state: state)
            
//            if epoch % 25 == 0 {
//                print("Epoch \(epoch): ∇ Loss = \(deltaLoss)  |  αA = \(attractAlpha.item(Float.self))  |  αR = \(repulsiveAlpha.item(Float.self))")
//                makeScatterPlot(embedding, dataSetName: "GPUMAP_epoch_\(epoch)")
//            }
        }
        
        //print("GPUMAP END")
        //makeScatterPlot(embedding, dataSetName: "GPUMAP_END")
    }
    
}

