//
//  Initial Values.swift
//  Curio
//
//  Created by Jim Wallace on 2025-03-09.
//

public enum InitialValues {
    case random
    case pca
    case laplacian
}
