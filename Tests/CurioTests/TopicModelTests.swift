#if arch(arm64)
import XCTest
import Foundation
import MLX
@testable import Curio

final class BasicTopicModelTests: XCTestCase {
    
    func testBasicTopicModel() async throws {
        var corpus = IndexedCorpus(item: Submission.self, encoder: await M2VEncoder(), metric: AngularDistance())
        await corpus.loadFromBundledNDJSONArchive("waterloo_submissions", withExtension: "zst", maxDocuments: 1000, in: Bundle.module)
        print(corpus)

        let topicModel = await BasicTopicModel(corpus: corpus,
                                               clusteringAlgorithm: KMeans(numTopics: 10),
                                               reducer: UMAP(targetDimensions: 10, numNeighbours: 30, minDistance: 1.0, initialValues: .pca)
        )
        print(topicModel)
        await topicModel.plot(topicModelName: "BasicTopicModel_waterloo_submissions")
    }
    
    func testBERTopic() async throws{
        var corpus = IndexedCorpus(item: Submission.self, encoder: CoreMLEncoder(), metric: EuclideanDistance())
        await corpus.loadFromBundledNDJSONArchive("waterloo_submissions", withExtension: "zst", maxDocuments: 2000, in: Bundle.module)
        print(corpus)

        let topicModel = await BERTopic(corpus: corpus)
        print(topicModel)
        await topicModel.plot(topicModelName: "BERTopic_waterloo_submissions")
    }
    
}
#endif

